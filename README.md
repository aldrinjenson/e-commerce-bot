# Retail - bot

Telegram e commerce bot

## Features

- Browse shops based on location(pincode)
- Browse categories and product available in selected shop
- Search for products across all nearby shops
- Add products to cart
- Feature for seller to set constraints when buying shops like greater than 1.5 kg etc
- Feature to order products
- Cancel orders which are not yet delivered
- Feature to get updates on status change of orders
- Add delivery location to generate google maps link
- Option to pay money using UPI or as Cash on Delivery
