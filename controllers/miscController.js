const { apiClient } = require("../utils/apiClient");
const { getAnswer } = require("../utils/lib");

const updatePin = async (msg, bot) => {
  const chatId = msg.chat.id;
  const [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  if (!customer || !customer.pinCode) {
    bot.sendMessage(chatId, "Please place an order for saving your pincode.");
    return;
  }
  const { pinCode } = await getAnswer(
    {
      key: "pinCode",
      prompt: "Please enter your updated PIN Code. Enter . to skip entering.",
      condition: (val) => val === "." || /^[0-9]{1,6}$/.test(val),
      formatter: (val) => (val === "." ? null : val),
    },
    chatId,
    bot
  );
  if (pinCode) {
    const updatedCustomer = await apiClient.patch("/customer", {
      tgUserId: chatId,
      pinCode,
    });
    if (updatedCustomer) {
      bot.sendMessage(
        chatId,
        "PIN Code has been updated.\nEnter /browse to browse shops near you"
      );
    }
  } else {
    bot.sendMessage(chatId, "Skipping PIN Code updation.");
  }
};

module.exports = { updatePin };
