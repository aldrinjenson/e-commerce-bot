const { cartEmptyMessage } = require("../constants");
const { apiClient } = require("../utils/apiClient");
const { handleProductsBuy } = require("../utils/cartUtils");

const checkoutItems = async (msg, bot) => {
  const chatId = msg.chat.id;
  let [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  if (!customer || !customer.cartItems.length) {
    bot.sendMessage(chatId, cartEmptyMessage);
    return;
  }
  const { cartItems } = customer;
  handleProductsBuy(msg, bot, cartItems);
};

module.exports = { checkoutItems };
