const fs = require("fs");
const path = require("path");
const { apiClient } = require("../utils/apiClient");
const {
  handleButtons,
  getAnswer,
  getAnswerFromButtonGroup,
} = require("../utils/lib");
const {
  stringifyProductItem,
  stringifyProductWithExtraDetails,
  toTitleCase,
} = require("../utils/miscUtils");
const { sliceAndListItems } = require("../utils/otherUtils");
const { addNewAddress } = require("../utils/productUtils");
const { checkoutItems } = require("./checkoutController");

const chooseDepartment = async (
  cbQuery,
  bot,
  company,
  listProducts,
  showDescription = true
) => {
  const chatId = cbQuery.message.chat.id;
  if (showDescription && company.description) {
    await bot.sendMessage(chatId, company.description);
  }
  const availableProducts = await apiClient.get("/product", {
    addedCompany: company._id,
  });
  let departmentsAvailable = availableProducts.map((el) => el.type);
  departmentsAvailable = [...new Set(departmentsAvailable)];

  const departmentButtons = departmentsAvailable.map((department) => ({
    text: toTitleCase(department),
    onPress: (cbQuery, bot) =>
      chooseProducts(cbQuery, bot, availableProducts, department, listProducts),
  }));
  const keyboardOptions = handleButtons(departmentButtons, chatId.toString());
  bot.sendMessage(chatId, "Choose category:", keyboardOptions);
};

const chooseProducts = async (
  { message },
  bot,
  products,
  department,
  listProducts
) => {
  const chatId = message.chat.id;
  const availableProducts = products.filter(
    (product) => product.type === department
  );
  const productsLength = availableProducts.length;
  await bot.sendMessage(
    chatId,
    productsLength +
      ` matching product${productsLength !== 1 ? "s" : ""} found.`
  );
  sliceAndListItems(availableProducts, chatId, bot, listProducts);
};

const fallbackImg = fs.readFileSync(
  path.resolve(__dirname, `../assets/${process.env.BOT_TYPE}NoPreview.png`)
);

const listProducts = (products, chatId, bot) => {
  return new Promise((resolve) => {
    products.forEach(async (product, i) => {
      let imgIndex = 0;
      let buttons = [
        {
          text: "←",
          onPress: (cbQuery, bot) =>
            updateImg(cbQuery, bot, product, --imgIndex),
        },
        {
          text: "→",
          onPress: (cbQuery, bot) =>
            updateImg(cbQuery, bot, product, ++imgIndex),
        },
        {
          text: "More Details",
          onPress: (cbQuery, bot) => showMoreDetails(cbQuery, bot, product),
        },
        {
          text: "Buy Now",
          onPress: (cbQuery, bot) => handleAddToCart(cbQuery, bot, product),
        },
      ];
      const imgLength = product.imgUrls.length;
      if (imgLength <= 1) {
        buttons = buttons.slice(2);
      }
      const uid = product._id;
      const keyboardOptions = handleButtons(buttons, uid);

      try {
        await bot.sendPhoto(chatId, product.imgUrls[0] || fallbackImg, {
          caption: stringifyProductItem(product),
          ...keyboardOptions,
        });
      } catch (error) {
        console.log("Error in sending photo: " + error);
        await bot.sendPhoto(chatId, fallbackImg, {
          caption: stringifyProductItem(product),
          ...keyboardOptions,
        });
      } finally {
        if (i === products.length - 1) {
          resolve();
        }
      }

      const updateImg = (cbQuery, bot, product, imgIndex) => {
        const { message_id: msgId, chat, caption } = cbQuery.message;
        const { imgUrls } = product;
        const index = imgIndex % imgUrls.length;
        const newImg = imgUrls[index] || imgUrls[imgUrls.length + index];
        bot.editMessageMedia(
          {
            type: "photo",
            media: newImg,
            caption,
            parse_mode: "HTML",
          },
          { chat_id: chat.id, message_id: msgId, ...keyboardOptions }
        );
      };

      const showMoreDetails = async (cbQuery, bot, product) => {
        const { message_id: msgId, chat } = cbQuery.message;
        const caption = await stringifyProductWithExtraDetails(product);
        bot.editMessageCaption(caption, {
          chat_id: chat.id,
          message_id: msgId,
          ...keyboardOptions,
        });
      };
    });
  });
};

const handleAddToCart = async (cbQuery, bot, product) => {
  const {
    message: { chat },
  } = cbQuery;
  const chatId = chat.id;
  const { name, baseQuantity, unit } = product;

  let [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  const [addedCompany] = await apiClient.get("/company", {
    _id: product.addedCompany,
  });

  if (!customer) {
    await bot.sendMessage(
      chatId,
      "Looks like this is the first time you are ordering with us."
    );
    const customerDetails = await addNewAddress(
      cbQuery.message,
      bot,
      addedCompany
    );
    customer = await apiClient.patch("/customer", {
      name: customerDetails.name,
      address: customerDetails.address,
      phoneNumber: customerDetails.phoneNumber,
      email: customerDetails.email,
      tgUserName: chat.username,
      tgUserId: chat.id,
      tgFullName: `${chat.first_name || ""} ${chat.last_name || ""}`,
      location: customerDetails.location,
      cartItems: [],
      pinCode: customerDetails.pinCode,
    });
  }
  const existingCartItems =
    customer.cartItems?.length && customer.cartItems[0].product
      ? customer.cartItems
      : [];
  if (
    existingCartItems.length &&
    existingCartItems[0].product.addedCompany !== product.addedCompany
  ) {
    bot.sendMessage(
      chatId,
      `You already have some items in your cart which are not from ${addedCompany.name}. Please check them out or remove them from cart using /cart before ordering ${product.name}`
    );
    return;
  }
  const sameItem = existingCartItems.find(
    (item) => item.product._id === product._id
  );
  const { quantity } = await getAnswer(
    {
      key: "quantity",
      prompt: `How many ${
        baseQuantity && baseQuantity != 1 ? baseQuantity : ""
      }${unit} of ${name} of do you want to buy?`,
      condition: (val) => Number.isInteger(+val) && +val > 0,
      formatter: (val) => +val,
    },
    chatId,
    bot
  );

  let updatedCartItems = [];
  if (sameItem) {
    bot.sendMessage(
      chatId,
      `${sameItem.quantity} ${product.name} already present in cart.\nIncrementing the quanity by ${quantity}.`
    );
    updatedCartItems = existingCartItems.map((item) => {
      if (item.product._id === product._id) {
        item.quantity += quantity;
      }
      return item;
    });
  } else {
    updatedCartItems = [
      ...existingCartItems,
      { product: product._id, quantity },
    ];
  }

  const updatedCustomer = await apiClient.patch("/customer", {
    ...customer,
    cartItems: updatedCartItems,
  });
  if (updatedCustomer) {
    await bot.sendMessage(
      chatId,
      `${product.name} has been added to your cart.`
    );
  }

  const confirmButtons = ["Yes", "No, Checkout"];
  const shouldAddMoreProduct = await getAnswerFromButtonGroup(
    {
      key: "shouldAddMoreProduct",
      prompt: `Do you want to add more products?`,
      buttons: confirmButtons,
      condition: (val) => confirmButtons.includes(val),
      formatter: (val) => val === confirmButtons[0],
    },
    chatId,
    bot
  );
  if (shouldAddMoreProduct) {
    chooseDepartment(cbQuery, bot, addedCompany, listProducts, false);
  } else {
    checkoutItems(cbQuery.message, bot);
  }
};

module.exports = {
  listProducts,
  chooseDepartment,
};
