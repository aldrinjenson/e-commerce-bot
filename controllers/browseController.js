const { handleButtons, getAnswer } = require("../utils/lib");
const { apiClient } = require("../utils/apiClient");
const { listProducts, chooseDepartment } = require("./productController");

const browseProducts = async (msg, bot) => {
  // if (company) {
  //   chooseDepartment(msg, bot, company, listProducts);
  // }
  const chatId = msg.chat.id;
  // try {
  // const allCompanies = await apiClient.get("/company", { hasProducts: true });
  let pc = null;
  const [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  if (!customer || !customer.pinCode) {
    const { pinCode } = await getAnswer(
      {
        key: "pinCode",
        prompt:
          "Please enter your PIN Code to find shops near you. Enter . if you want to skip and browse across all shops.",
        condition: (val) => val === "." || /^[0-9]{1,6}$/.test(val),
        formatter: (val) => (val === "." ? null : val),
      },
      chatId,
      bot
    );
    pc = pinCode;
  } else {
    pc = customer.pinCode;
  }
  bot.sendMessage(
    chatId,
    `Searching for shops${
      pc ? ` near PIN Code ${pc}` : ""
    }.\nTo update your location, enter /pincode.`
  );
  const allCompanies = await apiClient.get("/company", {
    hasProducts: true,
    pinCode: pc,
  });
  if (!allCompanies?.length) {
    bot.sendMessage(
      chatId,
      "No shops seem to be present near your location. Please try again using a different pinCode"
    );
    return;
  }
  const companyButtons = allCompanies.map((company) => ({
    text: company.name,
    onPress: (cbQuery, bot) =>
      chooseDepartment(cbQuery, bot, company, listProducts),
  }));
  const keyboardOptions = handleButtons(companyButtons, chatId.toString());
  bot.sendMessage(chatId, "Choose nearest shop:", keyboardOptions);
};

module.exports = { browseProducts };
