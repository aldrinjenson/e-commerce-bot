const { apiClient } = require("../utils/apiClient");
const { getAnswer } = require("../utils/lib");
const { sliceAndListItems } = require("../utils/otherUtils");
const { listProducts } = require("./productController");

const searchAndListProducts = async (bot, chatId) => {
  const { searchTerm } = await getAnswer(
    {
      key: "searchTerm",
      prompt: "Enter keyword to search products.",
    },
    chatId,
    bot
  );
  const [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  let pc = null;
  if (!customer || !customer.pinCode) {
    const { pinCode } = await getAnswer(
      {
        key: "pinCode",
        prompt:
          "Please enter your pinCode to find shops near you. Enter . if you want to skip and browse across all shops.",
        condition: (val) => val === "." || /^[0-9]{1,6}$/.test(val),
        formatter: (val) => (val === "." ? null : val),
      },
      chatId,
      bot
    );
    pc = pinCode;
  } else {
    pc = customer.pinCode;
  }
  bot.sendMessage(
    chatId,
    `Searching for ${searchTerm} near PIN Code ${pc}.\nTo update your location, enter /pincode.`
  );
  // const searchTerm = "Jackfruit basket";
  try {
    const matchingProducts = await apiClient.get("/product/search", {
      searchTerm,
      pinCode: pc,
    });
    if (!matchingProducts.length) {
      await bot.sendMessage(
        chatId,
        `No product with the entered query seem to be available near you. Please retry with a different search term.`
      );
      return searchAndListProducts(bot, chatId);
    }
    const productsCount = matchingProducts.length;
    await bot.sendMessage(
      chatId,
      productsCount +
        ` matching product${productsCount !== 1 ? "s" : ""} found.`
    );
    sliceAndListItems(matchingProducts, chatId, bot, listProducts);
  } catch (err) {
    console.log("error in search: " + err);
  }
};

module.exports = { searchAndListProducts };
