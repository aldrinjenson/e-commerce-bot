const { apiClient } = require("../utils/apiClient");
const {
  handleButtons,
  getAnswer,
  getAnswerFromButtonGroup,
} = require("../utils/lib");
const { handleMsgDelete } = require("../utils/miscUtils");
const { generateReceipt } = require("../utils/orderUtils");
const { sliceAndListItems } = require("../utils/otherUtils");

const handleCancelOrder = ({ message }, bot, order) => {
  const chatId = message.chat.id;
  const confirmButtons = [
    {
      text: "Yes, Cancel order",
      onPress: (cbQuery, bot) => {
        handleMsgDelete(cbQuery, bot);
        handleCancel(cbQuery, bot, order, message);
      },
    },
    {
      text: "No, keep the order",
      onPress: handleMsgDelete,
    },
  ];

  const keyboardOptions = handleButtons(confirmButtons, chatId.toString());

  const [{ product }, ...others] = order.products;
  const othersLength = others.length;
  bot.sendMessage(
    chatId,
    `Are you sure you want to cancel your order for ${product.brand || ""} ${
      product.name
    } ${othersLength ? ` and ${othersLength} others` : ""}?`,
    keyboardOptions
  );

  const handleCancel = async ({ message }, bot, order, orderMsg) => {
    const chatId = message.chat.id;
    const updatedOrder = await apiClient.patch("/order/status", {
      _id: order._id,
      status: "cancelled",
    });
    await bot.sendMessage(
      chatId,
      `Your order ${updatedOrder._id} has been cancelled.\nEnter /myorders to see full details`
    );
    bot.editMessageText(orderMsg.text, {
      chat_id: orderMsg.chat.id,
      message_id: orderMsg.message_id,
      parse_mode: "HTML",
    });

    const { feedback } = await getAnswer(
      {
        key: "feedback",
        prompt: "Please enter a feedback for cancellation of order",
      },
      chatId,
      bot
    );

    const udpatedRec = await apiClient.patch("/order/", {
      _id: order._id,
      feedback,
    });
    if (udpatedRec.feedback) {
      bot.sendMessage(
        chatId,
        "Thank you, your feedback has been received. Have a nice day:)"
      );
    }
  };
};

const listOrderItems = (orders, chatId, bot) => {
  return new Promise((resolve) => {
    orders.forEach(async (order, i) => {
      const receipt = generateReceipt(order);
      const orderButtons = [];
      const { status } = order;
      if (status !== "delivered" && status !== "cancelled")
        orderButtons.push({
          text: "Cancel Order",
          onPress: (cbQuery, bot) => handleCancelOrder(cbQuery, bot, order),
        });
      const keyboardOptions = handleButtons(orderButtons, order._id);
      await bot.sendMessage(chatId, receipt, keyboardOptions);
      if (i === orders.length - 1) {
        resolve();
      }
    });
  });
};

const listOrders = async (msg, bot) => {
  const chatId = msg.chat.id;
  const tgUserId = msg.from.id;

  const orderTypeButtons = ["Currently Live", "Delivered", "Cancelled"];
  const orderType = await getAnswerFromButtonGroup(
    {
      key: "orderType",
      prompt: "Choose order type",
      buttons: orderTypeButtons,
      condition: (val) => orderTypeButtons.includes(val),
      formatter: (val) => val.toLowerCase(),
    },
    chatId,
    bot
  );
  const { message_id } = await bot.sendMessage(chatId, "Loading orders..");
  const [customer] = await apiClient.get("/customer", { tgUserId });
  if (!customer) {
    bot.sendMessage(chatId, "You haven't made any orders.");
    return;
  }
  const orders = await apiClient.get("/order", { customer: customer._id });
  const filteredOrders = orderType.includes(" live")
    ? orders.filter(
        (order) => order.status !== "delivered" && order.status !== "cancelled"
      )
    : orders.filter((order) => order.status === orderType);

  const ordersLength = filteredOrders.length;
  await bot.editMessageText(
    ordersLength +
      ` ${orderType} order${ordersLength !== 1 ? "s" : ""} present.`,
    { chat_id: chatId, message_id }
  );

  sliceAndListItems(filteredOrders, chatId, bot, listOrderItems, 2);
};

module.exports = { listOrders };
