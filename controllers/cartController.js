const { cartEmptyMessage } = require("../constants");
const { apiClient } = require("../utils/apiClient");
const { handleButtons } = require("../utils/lib");
const { handleMsgDelete } = require("../utils/miscUtils");
const { stringifyCartItem } = require("../utils/otherUtils");
// const { listProducts } = require("./productController");

//////////////////////////////////////////////////////////////////////////////////////////

const handleQuantityUpdate = async (
  { message },
  bot,
  selectedProduct,
  operator
) => {
  const chatId = message.chat.id;
  const { reply_markup, message_id } = message;
  bot.editMessageText("Updating quantity", {
    chat_id: chatId,
    message_id,
  });
  const [customer] = await apiClient.get("/customer", { tgUserId: chatId });

  const updatedCartItems = customer.cartItems.map((item) => {
    if (item.product._id === selectedProduct._id) {
      let val = item.quantity + operator;
      if (val > 0) {
        item.quantity += operator;
      }
    }
    return item;
  });
  const updatedCustomer = await apiClient.patch("/customer", {
    tgUserId: chatId,
    cartItems: updatedCartItems,
  });

  const updatedCartItem = updatedCustomer.cartItems.find(
    ({ product }) => product._id === selectedProduct._id
  );
  const msg = stringifyCartItem(updatedCartItem);
  bot.editMessageText(msg, {
    chat_id: chatId,
    message_id: message.message_id,
    parse_mode: "HTML",
    reply_markup,
  });
};

/////////////////////////////////////////////////////////////////////////////////////////

const handleRemoveFromCart = async (cbQuery, bot, selectedProduct) => {
  const { message } = cbQuery;
  const chatId = message.chat.id;
  bot.editMessageText("Removing from cart...", {
    chat_id: chatId,
    message_id: message.message_id,
  });
  const [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  const updatedCartItems = customer.cartItems.filter(
    ({ product }) => product._id !== selectedProduct._id
  );
  await apiClient.patch("/customer", {
    tgUserId: chatId,
    cartItems: updatedCartItems,
  });
  handleMsgDelete(cbQuery, bot);
  bot.sendMessage(
    chatId,
    `${selectedProduct.name} has been removed from your cart`
  );
};

const listCartItems = async (msg, bot) => {
  const chatId = msg.chat.id;
  let [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  if (!customer || !customer.cartItems.length) {
    bot.sendMessage(chatId, cartEmptyMessage);
    return;
  }
  const { cartItems } = customer;
  for (let i = 0; i < cartItems.length; i++) {
    const { product } = cartItems[i];
    const caption = stringifyCartItem(cartItems[i]);

    const cartItemButtons = [
      {
        text: "-",
        onPress: (cbQuery, bot) =>
          handleQuantityUpdate(cbQuery, bot, product, -1),
      },
      {
        text: "+",
        onPress: (cbQuery, bot) =>
          handleQuantityUpdate(cbQuery, bot, product, +1),
      },
      {
        text: "Remove from cart",
        onPress: (cbQuery, bot) => handleRemoveFromCart(cbQuery, bot, product),
      },
    ];
    const keyboardOptions = handleButtons(cartItemButtons, product._id);
    await bot.sendMessage(chatId, caption, keyboardOptions);
  }
  await bot.sendMessage(
    chatId,
    "Enter /checkout to checkout with items in cart"
  );
};

module.exports = { listCartItems };
