const express = require("express");
const { CANCELLED, DELIVERED } = require("./constants");
const { handleProductDelivered } = require("./utils/orderUtils");
const router = express.Router();

const orderRouter = (bot) => {
  router.get("/", (req, res) => {
    res.send(
      "Bot active\nCurrent Time: " +
        new Date().toLocaleString("en-IN", {
          timeZone: "Asia/Calcutta",
        })
    );
  });

  router.post("/notify", (req, res) => {
    const { type, payload } = req.body;
    if (type === "STATUS_UPDATE") {
      const { customer, status, _id } = payload;
      switch (status) {
        case CANCELLED:
          break;
        case DELIVERED:
          handleProductDelivered(payload, bot);
          break;
        default:
          bot.sendMessage(
            customer.tgUserId,
            `Your order ${_id} has been ${status}.\nEnter /myorders to see full details`
          );
      }
      res.send({ msg: "OK", err: null });
    }
  });
  return router;
};

module.exports = { orderRouter };
