const CANCELLED = "cancelled";
const DELIVERED = "delivered";
const PENDING = "pending";
const DISPATCHED = "dispatched";

const cartEmptyMessage =
  "Your cart seems to be empty!\nTry making some orders using /browse to add items to your cart.";

module.exports = {
  CANCELLED,
  DELIVERED,
  PENDING,
  DISPATCHED,
  cartEmptyMessage,
};
