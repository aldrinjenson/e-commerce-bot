process.env.NTBA_FIX_319 = 1;
process.env.NTBA_FIX_350 = 1;
require("dotenv").config();
const TelegramBot = require("node-telegram-bot-api");
const express = require("express");
const { initializeLibFunctions } = require("./utils/lib");
const { searchAndListProducts } = require("./controllers/searchController");
const { browseProducts } = require("./controllers/browseController");
const { listOrders } = require("./controllers/orderController");
const { listCartItems } = require("./controllers/cartController");
const { checkoutItems } = require("./controllers/checkoutController");
const { updatePin } = require("./controllers/miscController");
const { orderRouter } = require("./routes");

// tg-bot begin
const bot = new TelegramBot(process.env.BOT_TOKEN, { polling: true });
initializeLibFunctions(bot);
bot.on("polling_error", console.log);
console.log("Bot active");

const app = express();
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "OPTIONS,GET,POST");
  res.setHeader("Access-Control-Allow-Headers", "*");
  next();
});
const PORT = process.env.PORT || 5001;
app.use(express.json());
app.use(orderRouter(bot));
app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));

const helpMessage = `This is a local retail bot which is aimed at making purchases from local stores nearby easy and convienient.\nCommands available:\n/browse: To search for shops and see browse their products\n/search: To search for products across shops using a search term.\n/help: To see this help message again.\n/myorders: To see the past orders made \n/support to know more about this bot and to contact support.`;
bot.onText(/\/start/, (msg) => {
  const chatId = msg.chat.id;
  const resp =
    "This is a local retail bot.\nEnter /browse to browse for shops and their products.\nEnter /search to search for any particular product across shops.\nEnter /help to see more information.";
  bot.sendMessage(chatId, resp);
});

bot.onText(/\/help/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, helpMessage);
});

bot.onText(/\/support/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(
    chatId,
    `This bot was made by the Open Kerala Initiative, as part of our aim to promote Open Networks for Digital Commerce. We try to make sure that businesses thrive locally and that the economy prospers by ensuring local businesses are discoverable to customers.
\nFor further details, visit openkerala.org.
\nOr reach out to us at : +91 7994506343.`
  );
});

bot.onText(/\/browse/, (msg) => {
  browseProducts(msg, bot);
});

bot.onText(/\/search/, async (msg) => {
  const chatId = msg.chat.id;
  searchAndListProducts(bot, chatId);
});

bot.onText(/\/myorders/, async (msg) => {
  listOrders(msg, bot);
});

bot.onText(/\/cart/, async (msg) => {
  listCartItems(msg, bot);
});

bot.onText(/\/checkout/, async (msg) => {
  checkoutItems(msg, bot);
});

bot.onText(/\/pincode/, async (msg) => {
  updatePin(msg, bot);
});
