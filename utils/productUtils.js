const { apiClient } = require("./apiClient");
const { handleButtons, getAnswer } = require("./lib");
const {
  stringifyUserDetails,
  handleMsgDelete,
  distanceInKmBetweenEarthCoordinates,
} = require("./miscUtils");

const MAX_DISTANCE_KMS = 25;
const locationValidator = (val, company) => {
  if (val === ".") return true;
  if (!val.latitude && !val.longitude) {
    return false;
  }
  const [long, lat] = company.location.coordinates;
  const diff = distanceInKmBetweenEarthCoordinates(
    lat,
    long,
    val.latitude,
    val.longitude
  );
  return diff <= MAX_DISTANCE_KMS;
};

const locationFormatter = (val) => {
  if (val === ".") return { type: "Point" };
  const { latitude, longitude } = val;
  return {
    type: "Point",
    coordinates: [longitude, latitude],
  };
};

const addNewAddress = async (message, bot, addedCompany) => {
  const { chat } = message;
  const chatId = chat.id;
  await bot.sendMessage(
    chatId,
    `Please enter your contact details for delivery. Edits can be made again at the end before final confirmation.`
  );

  const { locality, district, pinCode } = addedCompany;
  const customerDetails = await getAnswer(
    [
      {
        key: "name",
        prompt: "Enter your Full Name.",
        condition: (val) => /^[a-zA-Z\s]*$/.test(val),
      },
      {
        key: "phoneNumber",
        prompt:
          "Enter your 10-digit phone number without any spaces. eg: 9878987657.",
        condition: (val) => val.length === 10 && /^\d+$/.test(val),
      },
      // {
      //   key: "email",
      //   prompt: "Enter your email address. Enter . to skip adding",
      //   condition: (val) => val === "." || /^\S+@\S+\.\S+$/.test(val),
      //   formatter: (val) => (val === "." ? null : val),
      // },
      {
        key: "address",
        prompt: "Enter delivery address.",
        errorMsg: "Invalid address with less than 10 characters",
        condition: (val) => val.length > 10,
      },
      {
        key: "pinCode",
        prompt: "Enter your pin code.",
        condition: (val) => /^[0-9]{1,6}$/.test(val),
      },
      {
        key: "location",
        type: "location",
        prompt:
          "Please turn on your GPS and share the delivery location using the Telegram attachment icon for easy delivery.\n\nIf you are on Telegram desktop or is unable to do so, enter . to skip. We'll get in touch with you.",
        condition: (val) => locationValidator(val, addedCompany),
        errorMsg: `Please choose a location which is within ${MAX_DISTANCE_KMS} kilometer distance of ${locality} ${district}, ${pinCode} `,
        formatter: locationFormatter,
      },
    ],
    chat.id,
    bot
  );
  return new Promise((resolve) => {
    const confirmButtons = [
      {
        text: "Yes, Proceed",
        onPress: (cbQuery, bot) => {
          handleMsgDelete(cbQuery, bot);
          resolve(customerDetails);
        },
      },
      {
        text: "No, Make edits",
        onPress: ({ message }, bot) =>
          resolve(addNewAddress(message, bot, addedCompany)),
      },
    ];
    const keyboardOptions = handleButtons(confirmButtons);
    bot.sendMessage(
      chat.id,
      "Confirm details?\n" + stringifyUserDetails(customerDetails),
      keyboardOptions
    );
  });
};

const getCustomerDetails = async ({ message, from }, bot, product) => {
  const [existingCustomer] = await apiClient.get("/customer", {
    tgUserId: from.id,
  });
  const [addedCompany] = await apiClient.get("/company", {
    _id: product.addedCompany,
  });

  let couldSkipEnteringAddress =
    existingCustomer &&
    existingCustomer.pinCode &&
    // existingCustomer.email &&
    existingCustomer.location &&
    existingCustomer.location.coordinates;

  if (couldSkipEnteringAddress) {
    const [longitude, latitude] = existingCustomer.location.coordinates;
    couldSkipEnteringAddress = locationValidator(
      { latitude, longitude },
      addedCompany
    );
  }

  if (couldSkipEnteringAddress) {
    return new Promise((resolve) => {
      const confirmButtons = [
        {
          text: "Yes, Proceed",
          onPress: (cbQuery, bot) => {
            handleMsgDelete(cbQuery, bot);
            resolve(existingCustomer);
          },
        },
        {
          text: "No, Make edits",
          onPress: (cbQuery, bot) => {
            handleMsgDelete(cbQuery, bot);
            resolve(addNewAddress(message, bot, addedCompany));
          },
        },
      ];
      const keyboardOptions = handleButtons(confirmButtons);
      bot.sendMessage(
        message.chat.id,
        "Use the details from your last purchase?\n" +
          stringifyUserDetails(existingCustomer),
        keyboardOptions
      );
    });
  } else {
    return addNewAddress(message, bot, addedCompany);
  }
};

module.exports = {
  addNewAddress,
  getCustomerDetails,
};
