const { cartEmptyMessage } = require("../constants");
const { apiClient } = require("./apiClient");
const {
  getPopupKeyboard,
  getAnswer,
  handleButtons,
  getAnswerFromButtonGroup,
} = require("./lib");
const { handleMsgDelete } = require("./miscUtils");
const { generateReceipt } = require("./orderUtils");
const { getCustomerDetails } = require("./productUtils");

const handleProductsBuy = async (message, bot) => {
  const chatId = message.chat.id;
  const [customer] = await apiClient.get("/customer", { tgUserId: chatId });
  const cartItems = customer.cartItems;
  if (!cartItems) {
    bot.sendMessage(chatId, cartEmptyMessage);
  }
  const confirmButtons = [
    {
      text: "Yes, Proceed",
      onPress: (cbQuery, bot) => {
        handleMsgDelete(cbQuery, bot);
        handleBuy(cbQuery, bot, cartItems);
      },
    },
    {
      text: "No, Cancel",
      onPress: (cbQuery, bot) => {
        bot.sendMessage(
          cbQuery.message.chat.id,
          "Cancelling purchase.\nYou can checkout at anytime using /checkout ."
        );
        // chooseDepartment(cbQuery, bot, addedCompany, listProducts, false);
        handleMsgDelete(cbQuery, bot);
      },
    },
  ];

  const keyboardOptions = handleButtons(confirmButtons, chatId.toString());
  let confirmMsg = "Do you want to confirm buying the following?\n";
  cartItems.forEach(({ product, quantity }) => {
    const { baseQuantity, unit } = product;
    const shouldShowBrand = process.env.BOT_TYPE !== "fruit" && product.brand;
    confirmMsg += `* ${shouldShowBrand ? product.brand : ""} ${
      product.name
    } (${quantity}${
      baseQuantity && baseQuantity !== 1 ? " x " + baseQuantity : ""
    } ${unit})\n`;
  });
  bot.sendMessage(chatId, confirmMsg, keyboardOptions);
};

const handleBuy = async (cbQuery, bot, cartItems) => {
  const firstProd = cartItems[0].product;
  const [addedCompany] = await apiClient.get("/company", {
    _id: firstProd.addedCompany,
  });
  const chatId = cbQuery.message.chat.id;
  const customerDetails = await getCustomerDetails(cbQuery, bot, firstProd);

  const paymentButtons = ["Cash on Delivery"];
  if (addedCompany.upi && addedCompany.upiPhoneNumber) {
    paymentButtons.push("UPI");
  }

  const extraDetails = [
    {
      key: "paymentMethod",
      prompt: "Choose preferred payment method",
      keyboard: getPopupKeyboard(paymentButtons),
      condition: (val) => paymentButtons.includes(val),
    },
  ];
  const { deliverySlots } = addedCompany;
  if (deliverySlots?.length) {
    extraDetails.push({
      key: "preferredDeliverySlot",
      prompt: "Choose delivery Slot",
      keyboard: getPopupKeyboard(deliverySlots),
      condition: (val) => deliverySlots.includes(val),
    });
  }

  const { paymentMethod, preferredDeliverySlot } = await getAnswer(
    extraDetails,
    chatId,
    bot
  );

  let totalPrice = 0,
    priceOfItem = 0;
  for (let i = 0; i < cartItems.length; i++) {
    const { product, quantity } = cartItems[i];
    const { discountedPrice, price } = product;
    priceOfItem = discountedPrice ? discountedPrice : price;
    totalPrice += priceOfItem * quantity;
  }

  let stringifiedOrderDetails = generateReceipt(
    {
      products: cartItems,
      paymentMethod,
      totalPrice,
      deliverySlot: preferredDeliverySlot,
      company: addedCompany,
    },
    customerDetails
  );
  const confirmButtons = ["Yes, Proceed", "No, Keep browsing"];
  const confirmOrder = await getAnswerFromButtonGroup(
    {
      key: "confirmOrder",
      prompt: `${stringifiedOrderDetails}\n\nConfirm everything and place order?`,
      buttons: confirmButtons,
      condition: (val) => confirmButtons.includes(val),
      formatter: (val) => val === confirmButtons[0],
    },
    chatId,
    bot
  );
  if (!confirmOrder) {
    bot.sendMessage(
      chatId,
      "Order cancelled. Continue browsing using /browse ."
    );
    return;
  }
  const { from } = cbQuery;
  try {
    const customer = await apiClient.patch("/customer", {
      name: customerDetails.name,
      address: customerDetails.address,
      phoneNumber: customerDetails.phoneNumber,
      email: customerDetails.email,
      tgUserName: from.username,
      tgUserId: from.id,
      tgFullName: `${from.first_name || ""} ${from.last_name || ""}`,
      location: customerDetails.location,
      cartItems: [],
      pinCode: customerDetails.pinCode,
    });
    const order = await apiClient.post("/order", {
      paymentMethod,
      product: firstProd,
      products: cartItems,
      customer: customer._id,
      company: addedCompany._id,
      status: "pending",
      orderedBy: customer.name,
      orderedPhoneNo: customer.phoneNumber,
      orderedAddress: customer.address,
      orderedEmail: customerDetails.email,
      location: customerDetails.location,
      deliverySlot: preferredDeliverySlot || "",
      totalPrice,
    });
    // const receipt = generateReceipt(order, product);
    await bot.sendMessage(
      chatId,
      `Your order has been successfully placed. For future reference, use the following receipt:\n\nOrder Id:${order._id}\n${stringifiedOrderDetails}.`,
      {
        parse_mode: "HTML",
      }
    );
    if (
      paymentMethod === "UPI" &&
      addedCompany.upi &&
      addedCompany.upiPhoneNumber
    ) {
      const { name, upi, upiPhoneNumber } = order.company;
      bot.sendMessage(
        chatId,
        `You can visit the following link for further transactions with ${name}:\nUPI ID: ${upi}\nUPI Number: ${upiPhoneNumber}`,
        {
          reply_markup: {
            inline_keyboard: [
              [
                {
                  text: "Pay Now",
                  url: `https://upi-openkerala.herokuapp.com/${upi}/${totalPrice}`,
                },
              ],
            ],
          },
        }
      );
    }
  } catch (error) {
    console.log("Error in making order " + error);
  }
};
module.exports = {
  handleProductsBuy,
};
