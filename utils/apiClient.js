const axios = require("axios");
const { BASE_URL } = require("../config");

class ApiClient {
  async get(route, params) {
    try {
      const { data, err } = (
        await axios.get(`${BASE_URL}${route}`, {
          params,
        })
      ).data;
      if (err) {
        throw err;
      }
      return data;
    } catch (err) {
      console.log("Error with api: " + err);
    }
  }
  async post(route, params) {
    try {
      const val = (await axios.post(`${BASE_URL}${route}`, params)).data;
      const { data, err } = val;
      if (err) {
        throw err;
      }
      return data;
    } catch (err) {
      console.log("Error with api: " + err);
    }
  }
  async patch(route, params) {
    try {
      const val = (await axios.patch(`${BASE_URL}${route}`, params)).data;
      const { data, err } = val;
      if (err) {
        throw err;
      }
      return data;
    } catch (err) {
      console.log("Error with api: " + err);
    }
  }
}

const apiClient = new ApiClient();

module.exports = { apiClient };
