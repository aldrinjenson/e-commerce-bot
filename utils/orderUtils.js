const { apiClient } = require("./apiClient");
const { getAnswer } = require("./lib");
const {
  stringifyCompanyAddress,
  stringifyUserDetails,
} = require("./miscUtils");
const { stringifyCartItem } = require("./otherUtils");

const handleProductDelivered = async (order, bot) => {
  const { customer, _id, product } = order;
  const chatId = customer.tgUserId;
  await bot.sendMessage(
    chatId,
    `Your order ${_id} for ${product.name} has been successfully delivered`
  );

  const { feedback } = await getAnswer(
    {
      key: "feedback",
      prompt: "Please enter a feedback for the overall delivery process",
    },
    chatId,
    bot
  );
  const udpatedRec = await apiClient.patch("/order/", {
    _id: order._id,
    feedback,
  });
  if (udpatedRec) {
    console.log("feedback updated");
    bot.sendMessage(
      chatId,
      "Thank you, your feedback has been received. Have a nice day:)"
    );
  }
};

const generateReceipt = (order, customerDetails = null) => {
  const {
    products,
    paymentMethod,
    deliverySlot,
    totalPrice,
    company,
    createdAt,
  } = order;

  const customer = customerDetails || {
    name: order.orderedBy,
    phoneNumber: order.orderedPhoneNo,
    address: order.orderedAddress,
    email: order.orderedEmail,
    location: order.location,
  };

  let msg = `<strong>Product(s):</strong>\n${stringifyCartItem(
    products,
    true
  )}\n<strong>Sold by:\n</strong>${stringifyCompanyAddress(
    company
  )}\n\n<strong>Total Price: </strong>${totalPrice}Rs\n<strong>Payment Method: </strong>${paymentMethod}\n\n<strong>Delivery Details:</strong>\n${stringifyUserDetails(
    customer
  )}`;
  if (createdAt) {
    msg += `\nOrdered Time: ${new Date(createdAt).toLocaleString("en-IN", {
      timeZone: "Asia/Calcutta",
    })}`;
  }
  if (deliverySlot) {
    msg += `\n<strong>Delivery Slot: </strong>${deliverySlot}\n`;
  }
  return msg;
};

module.exports = { handleProductDelivered, generateReceipt };
