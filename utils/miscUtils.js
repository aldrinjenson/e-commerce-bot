const { apiClient } = require("./apiClient");
const botType = process.env.BOT_TYPE;

const handleMsgDelete = ({ message }, bot) => {
  const { chat, message_id } = message;
  bot.deleteMessage(chat.id, message_id);
};

const distanceInKmBetweenEarthCoordinates = (lat1, lon1, lat2, lon2) => {
  const earthRadiusKm = 6371;
  const degreesToRadians = (degrees) => {
    return (degrees * Math.PI) / 180;
  };
  const dLat = degreesToRadians(lat2 - lat1);
  const dLon = degreesToRadians(lon2 - lon1);
  lat1 = degreesToRadians(lat1);
  lat2 = degreesToRadians(lat2);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return earthRadiusKm * c;
};

const toTitleCase = (str) => {
  return str[0].toUpperCase() + str.slice(1);
};

const stringifyProductItem = (product, hideSeller = false) => {
  const {
    brand,
    name,
    price,
    discountedPrice,
    companyName,
    type,
    baseQuantity,
    unit,
  } = product;

  let mrp = price;
  if (discountedPrice) {
    mrp = `<strike>${price}</strike> ${discountedPrice}`;
  }

  if (baseQuantity && unit) {
    //will not likely be present for e-gadget's bot
    mrp += ` for ${baseQuantity}${unit}`;
  }

  const shouldShowBrand = botType !== "fruit" && brand;
  const b = shouldShowBrand ? brand : "";
  const c = shouldShowBrand ? "\nBrand: " + brand + " " : "";
  let str = `${b}${name}\nMRP: Rs ${mrp}${c}\nCategory: ${toTitleCase(type)}`;
  if (!hideSeller) {
    str += `\nSold by: ${companyName}`;
  }
  return str;
};

const stringifyUserDetails = (details = {}) => {
  const { name, phoneNumber, address, email, location, pinCode } = details;
  let str = `Name: ${name}\nPhone Number: ${phoneNumber}${
    email ? "\nE-Mail: " + email : ""
  }\nAddress: ${address}${pinCode ? "\nPIN code: " + pinCode : ""}`;
  if (location?.coordinates) {
    const [lng, lat] = location.coordinates;
    str += `\nLocation: https://maps.google.com/?q=${lat},${lng}`;
  }
  return str;
};

const stringifyCompanyAddress = (company) => {
  const { district, pinCode, locality, state, name: cName } = company;
  return `${cName}, ${locality}\n${district}, ${state},\nPIN: ${pinCode}`;
};

const stringifyProductWithExtraDetails = async (product) => {
  const { description, addedCompany } = product;
  const [company] = await apiClient.get("/company", { _id: addedCompany }); // getting the first record
  const { phoneNo } = company;

  let str =
    stringifyProductItem(product) +
    `\n\nDescription: ${description}\n\nSold by: ${stringifyCompanyAddress(
      company
    )}
    \nFor further details, contact: ${phoneNo} `;
  return str;
};

module.exports = {
  handleMsgDelete,
  stringifyProductItem,
  stringifyCompanyAddress,
  stringifyUserDetails,
  stringifyProductWithExtraDetails,
  toTitleCase,
  distanceInKmBetweenEarthCoordinates,
};
