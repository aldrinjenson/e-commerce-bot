const { handleButtons } = require("./lib");
const { stringifyProductItem, handleMsgDelete } = require("./miscUtils");

const stringifyCartItem = (cartItems, hideSeller) => {
  const items = Array.isArray(cartItems) ? cartItems : [cartItems];
  let msg = "";

  for (let i = 0; i < items.length; i++) {
    const { product, quantity } = items[i];
    msg += stringifyProductItem(product, hideSeller);
    const { discountedPrice, price, baseQuantity, unit } = product;
    const cost = discountedPrice ? discountedPrice : price;
    const mrp = `${quantity} x ${cost}Rs = ${cost * quantity}Rs\n`;
    msg += `\nQuantity: ${quantity} ${
      baseQuantity && baseQuantity !== 1 ? " x " + baseQuantity : ""
    } ${unit}\nPrice: ${mrp}`;
    if (i !== items.length - 1) {
      msg += "\n";
    }
  }

  return msg;
};

const sliceAndListItems = async (
  items = [],
  chatId,
  bot,
  listItemsFunc,
  LIMIT = 3
) => {
  const firstFiveProducts = items.slice(0, LIMIT);
  await listItemsFunc(firstFiveProducts, chatId, bot);

  if (items.length > LIMIT) {
    const restProducts = items.slice(LIMIT);
    const diff = Math.min(restProducts.length, LIMIT);
    const viewMoreButtons = [
      {
        text: `View next ${diff}`,
        onPress: (cbQuery, bot) => {
          handleMsgDelete(cbQuery, bot);
          const chatId = cbQuery.message.chat.id;
          sliceAndListItems(restProducts, chatId, bot, listItemsFunc, LIMIT);
        },
      },
    ];
    const keyboardOptions = handleButtons(viewMoreButtons);
    const itemsLeft = items.length - LIMIT;
    bot.sendMessage(chatId, `${itemsLeft} more available.`, keyboardOptions);
  }
};

module.exports = { stringifyCartItem, sliceAndListItems };
